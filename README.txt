A small module that uses hook_page_alter() to hide Drupal's core toolbar for
any users with the permission to access the admin_menu, preventing multiple
toolbars from appearing.
